﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JLTMEntProj.Models
{
    /// <summary>
    /// Getters and setters for the data in shopping cart order list
    /// </summary>
    /// <author>Tuan Mai</author>

    public class ShoppingCartOrder
    {
        public Product Product { get; set; }

        public int Quantity { get; set; }

        public decimal Subtotal
        {
            get
            {   // To return the calculate subtotal
                return Product.Price * Quantity;
            }
        }
    }
}