﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace JLTMEntProj.Models
{
    /// <summary>
    /// ShoppingCart class to handle the list of products in the cart
    /// </summary>
    /// <author>Tuan Mai</author>
     
    public class ShoppingCart
    {
        // Declaring field variables
        private List<ShoppingCartOrder> _scOrderList;
        Product _product;

        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// IEnumerable for OrderList of type ShoppingCartOrder class
        /// </summary>
        public IEnumerable<ShoppingCartOrder> OrderList
        {
            get
            {
                return _scOrderList;        // Return the shopping cart order list
            }
        }

        /// <summary>
        /// Constructor for ShoppingCart
        /// </summary>
        public ShoppingCart()
        {   
            _scOrderList = new List<ShoppingCartOrder>();   // Initialize Shopping Cart Order List 
        }

        /// <summary>
        /// AddProduct method to add an item from the shop to user's shopping cart
        /// </summary>
        /// <param name="product"></param>
        /// <param name="quantity"></param>
        public void AddProduct(Product product, int quantity)
        {
            // Declare and intialize local variables
            bool isSameProduct = false;
            bool isNewProduct = false;
            bool quantityExceedsInven = false;

            _product = db.Products.Find(product.ProductId);
            int dbProductQuantity = _product.Quantity;

            // If the order list is empty
            if (_scOrderList.Count == 0)
            {
                if (dbProductQuantity == 0)
                {
                    quantityExceedsInven = true;
                }
                else
                {
                    // Add the product to the order list
                    _scOrderList.Add(new ShoppingCartOrder { Product = product, Quantity = quantity });
                }
            }

            // Check if the order list is not empty
            else if (_scOrderList.Count > 0)
            {
                int orderListIndex;     // Declaring the indexer for order list

                // Run through all products in order list
                for (orderListIndex = 0; orderListIndex < _scOrderList.Count(); orderListIndex++)
                {
                    // If there is a product in cart already that we're trying to add again
                    if (_scOrderList[orderListIndex].Product.Name == product.Name)
                    {
                        if (dbProductQuantity == 0)
                        {
                            quantityExceedsInven = true;
                            break;
                        }

                        // If the shopping cart contains less than or equal to quantity in database
                        else if (_scOrderList[orderListIndex].Quantity < dbProductQuantity)
                        {
                            isSameProduct = true;   // Flag that it is same product
                            break;
                        }

                        else
                        {   // Else, flag that user trying to add more than what inventory has
                            quantityExceedsInven = true;
                            break;
                        }
                    }            
                }

                isNewProduct = true;    // Flag new product if did not find same products

                // If it is the same product
                if (isSameProduct == true)
                {   
                    // Increase the quantity of the product instead of adding a new one to list
                    _scOrderList[orderListIndex].Quantity = _scOrderList[orderListIndex].Quantity + 1;
                }

                // Else if the product you're  trying to add ends up with more than what the inventory of store has
                else if (quantityExceedsInven == true)
                {
                    // TODO: Display to user , letting them know they're trying to add more than inventory
                    string error = "Quantity exceeded.";
                    Debug.WriteLine("Quantity exceeded.");
                }

                // Else if the product is not already in cart
                else if (isNewProduct == true)
                {   
                    // Add the product to the list
                    _scOrderList.Add(new ShoppingCartOrder { Product = product, Quantity = quantity });
                }
            }           
        }

        /// <summary>
        /// RemoveProduct method for user to remove unwanted products from their shopping cart
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="quantity"></param>
        /// <returns>Returns this</returns>
        public ShoppingCart RemoveProduct(int productId, int quantity)
        {
            // Run through all products in the order list
            for (int orderListIndex = 0; orderListIndex < _scOrderList.Count(); orderListIndex++)
            {
                // If the cart contains more than 1
                if (_scOrderList[orderListIndex].Product.ProductId == productId && _scOrderList[orderListIndex].Quantity > 1)
                {
                    // Subtract 1 from the shopping cart
                    _scOrderList[orderListIndex].Quantity = _scOrderList[orderListIndex].Quantity - quantity;
                }

                // Else if there is one 1 of that product in the shopping cart
                else if (_scOrderList[orderListIndex].Product.ProductId == productId)
                {
                    // Remove that product from the cart
                    _scOrderList.RemoveAll(prod => prod.Product.ProductId == productId);
                }
            }

            return this;
        }

        /// <summary>
        /// UpdateInventory method to update the inventory after user pays for items
        /// </summary>
        public void UpdateInventory()
        {
            // For each product in shopping cart, remove quantity bought from database
            for (int orderListIndex = 0; orderListIndex < _scOrderList.Count(); orderListIndex++)
            {
                // Variable to store the quantity of product in shopping cart
                int quantityBought = _scOrderList[orderListIndex].Quantity;

                // Product object for product in database that is in accordance to product in shopping cart
                Product product = db.Products.Find(_scOrderList[orderListIndex].Product.ProductId);

                // If the quantity the user buys is less than the quantity in the database
                if (quantityBought < product.Quantity)
                {
                    // Subtract quantity bought from the quantity for that product in the database
                    product.Quantity = product.Quantity - quantityBought;
                    db.SaveChanges();
                }

                // Else if the quantity buying is all that's left in inventory
                else if (quantityBought == product.Quantity)
                {
                    product.Quantity = 0;   // Set quantity of that product in inventory to 0
                    db.SaveChanges();
                }
            }
        }
    }
}