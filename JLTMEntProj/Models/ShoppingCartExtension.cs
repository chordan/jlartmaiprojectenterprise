﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JLTMEntProj.Models
{
    /// <summary>
    /// Extention class to ShoppingCart to handle extention methods
    /// </summary>
    /// <author>Tuan Mai</author>

    public static class ShoppingCartExtension
    {
        // Declare and intialize the tax for total
        private const decimal TAX = 1.13m;

        /// <summary>
        /// OrderSubtotal method to calculate the total subtotal of all items.
        /// </summary>
        /// <param name="cart"></param>
        /// <returns subtotal></returns>
        public static decimal OrderSubtotal(this ShoppingCart cart)
        {
            decimal subtotal = 0;

            foreach (ShoppingCartOrder order in cart.OrderList)
            {   // Foreach order in the list, get the entire subtotal of all the products
                subtotal += order.Subtotal;
            }

            return subtotal;
        }

        /// <summary>
        /// OrderTaxAmount method to calculate the total taxable amount that user pays
        /// </summary>
        /// <param name="cart"></param>
        /// <returns taxAmount></returns>
        public static decimal OrderTaxAmount(this ShoppingCart cart)
        {
            decimal subtotal = 0;
            decimal total = 0;
            decimal taxAmount = 0;

            foreach (ShoppingCartOrder order in cart.OrderList)
            {   // Foreach order in the list, get the entire subtotal, the final total, and subtract total-subtotal 
                // to get the payable tax amount
                subtotal += order.Subtotal;
                total += order.Subtotal * TAX;

                taxAmount = total - subtotal;
            }

            return taxAmount;
        }

        /// <summary>
        /// OrderTotal method to calculate the total amount that the user has to pay
        /// </summary>
        /// <param name="cart"></param>
        /// <returns total></returns>
        public static decimal OrderTotal(this ShoppingCart cart)
        {
            decimal total = 0;

            foreach (ShoppingCartOrder order in cart.OrderList)
            {   // Foreach order in the list, get total subtotal and multiply it by the tax
                total += order.Subtotal * TAX;
            }

            return total;
        }
    }
}