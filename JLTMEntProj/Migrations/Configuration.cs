namespace JLTMEntProj.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<JLTMEntProj.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            AddUserAndRole(context);
            context.Products.AddOrUpdate(p => p.Name,
               new Product
               {
                   ProductId=1,
                   Name = "Jordan's Desk",
                   Description="This is a very long description, isn't it?",
                   Price=2.99m,
                   Quantity=11
               },
               new Product
               {
                   ProductId = 2,
                   Name = "Tuan's Food",
                   Description = "Why are you stealing my food",
                   Price = 10.50m,
                   Quantity = 4
               }

                );
            context.SaveChanges();
        }

        bool AddUserAndRole(ApplicationDbContext context)
        {
            IdentityResult ir;
            // Add a comment to this line
            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            ir = rm.Create(new IdentityRole("canEdit"));
            var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser()
            {
                UserName = "jordan@larock.ca",
            };
            ir = um.Create(user, "Larock_1");
            if (ir.Succeeded == false)
                return ir.Succeeded;
            ir = um.AddToRole(user.Id, "canEdit");
            return ir.Succeeded;
        }
    }
}
