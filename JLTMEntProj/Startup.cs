﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JLTMEntProj.Startup))]
namespace JLTMEntProj
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
