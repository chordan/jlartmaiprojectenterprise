﻿using JLTMEntProj.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JLTMEntProj.Controllers
{
    /// <summary>
    /// Controller for the shopping of the web
    /// </summary>
    /// <author>Tuan Mai</author>

    public class ShoppingController : Controller
    {
        // Declare Constant for shopping cart obj
        private const string SHOPPING_CART_SESSION_OBJ = "ShoppingCart";

        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Getter for the shopping cart via session of shopping cart object
        /// </summary>
        public ShoppingCart ShoppingCart
        {
            get
            {
                ShoppingCart cart = Session[SHOPPING_CART_SESSION_OBJ] as ShoppingCart;

                if (cart == null)
                {   // Set new session to shopping cart object
                    cart = new ShoppingCart();
                    Session[SHOPPING_CART_SESSION_OBJ] = cart;
                }

                return cart;
            }
        }

        /// <summary>
        /// Shop ActionResult to display all the products in the shop
        /// Sorting functionality
        /// </summary>
        /// <returns>Returns View</returns>
        public ActionResult Shop(string sortOrder, string searchString)
        {
            var products = from p in db.Products
                           select p;

            if (!String.IsNullOrEmpty(searchString))
            {   // Search through products
                products = db.Products.Where(p => p.Name.Contains(searchString)
                                       || p.Description.Contains(searchString));
            }

            switch (sortOrder)
            {   // The sort order
                case "name":
                    products = products.OrderBy(p => p.Name);
                    break;
                case "desc":
                    products = products.OrderBy(p => p.Description);
                    break;
                case "quan":
                    products = products.OrderBy(p => p.Quantity);
                    break;
                case "price":
                    products = products.OrderBy(p => p.Price);
                    break;
                default:
                    products = products.OrderBy(p => p.Name);
                    break;
            }

            return View(products.ToList());
        }

        /// <summary>
        /// ViewCart ActionResult to display the list of items in the shopping cart
        /// </summary>
        /// <returns View(ShoppingCart)></returns>
        public ActionResult ViewCart()
        {
            return View(ShoppingCart);
        }

        /// <summary>
        /// AddToCart ActionResult to add the product to the user's shopping cart
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>RedirectsToAction</returns>>
        public ActionResult AddToCart(int productId)
        {
            // Search the database for product with same id
            Product product = db.Products.Find(productId);

            if (product != null)
            {
                ShoppingCart.AddProduct(product, 1);    // Call method to add this product to shopping cart
            }

            return RedirectToAction("ViewCart");
        }

        /// <summary>
        /// RemoveFromCart ActionResult to remove the product from the shopping cart that user selected
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>Returns View</returns>
        public ActionResult RemoveFromCart(int productId)
        {
            ShoppingCart.RemoveProduct(productId, 1);

            return View("ViewCart", ShoppingCart);
        }

        /// <summary>
        /// ProceedToCheckout ActionResult to handle the Checkout view
        /// </summary>
        /// <returns>Returns View</returns>
        public ActionResult ProceedToCheckout()
        {
            return View("CheckoutView", ShoppingCart);
        }

        /// <summary>
        /// PaymentOptions ActionResult to display view for users to pay
        /// </summary>
        /// <returns>Returns View</returns>
        public ActionResult PaymentOptions()
        {
            return View("Pay", ShoppingCart);
        }

        /// <summary>
        /// Paid ActionResult calls UpdateInventory to update inventory after user pays
        /// </summary>
        /// <returns>RedirectsToAction</returns>
        public ActionResult Paid()
        {
            ShoppingCart.UpdateInventory();

            Session.Remove(SHOPPING_CART_SESSION_OBJ);

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Dispose method to dispose of the database object
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}